package fil.coo.flopboxagent.data;

import fil.coo.flopboxagent.configuration.Configuration;
import fil.coo.flopboxagent.flopbox.FlopboxService;
import fil.coo.flopboxagent.flopbox.FtpServer;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;

/**
 * The LocalWatcherService is responsible for watching local files change and act on them in the flopbox server
 */
public class LocalWatcherService extends Thread implements FileAlterationListener {

    public LocalWatcherService() {

    }

    /**
     * Listen for local files changes via the WatchService API
     */
    public void run() {
        File directory = new File(Configuration.instance.getDataDirectoryPath());
        FileAlterationObserver observer = new FileAlterationObserver(directory);
        observer.addListener(this);
        while (true) {
            try {
                observer.checkAndNotify();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart(FileAlterationObserver observer) {
    }

    @Override
    public void onDirectoryCreate(File directory) {

    }

    @Override
    public void onDirectoryChange(File directory) {

    }

    @Override
    public void onDirectoryDelete(File directory) {

    }

    @Override
    public void onFileCreate(File file) {
        FlopboxService.upload(file);
    }

    @Override
    public void onFileChange(File file) {
        FlopboxService.upload(file);
    }

    @Override
    public void onFileDelete(File file) {
        FlopboxService.moveToDeletedFolder(file);
    }

    @Override
    public void onStop(FileAlterationObserver observer) {

    }

    public static void initializeDirectories(FtpServer[] ftpServers) {
        for (FtpServer ftpServer : ftpServers) {
            File serverDirectory = new File(Configuration.instance.getDataDirectoryPath() + "/" + ftpServer.getAlias());
            if (!serverDirectory.exists()) {
                serverDirectory.mkdir();
            }
        }
    }
}
