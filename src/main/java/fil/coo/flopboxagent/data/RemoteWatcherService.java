package fil.coo.flopboxagent.data;

import fil.coo.flopboxagent.configuration.Configuration;
import fil.coo.flopboxagent.flopbox.FlopboxService;
import fil.coo.flopboxagent.flopbox.FtpFileDto;

import java.io.IOException;

/**
 * The RemoteWatcherService class polls the flopbox server's for detecting changes, and manages files in the local
 * data directory
 */
public class RemoteWatcherService extends Thread {

    public RemoteWatcherService() {
    }

    /**
     * Listen for local files changes via the WatchService API
     */
    public void run() {
        System.out.println("Listening for remote changes...");
        while (true) {
            try {
                String alias = "ubuntu";
                FtpFileDto[] ftpFileDtos = FlopboxService.listFiles(alias, "");
                for (FtpFileDto f : ftpFileDtos) {
                    String localFilePath = Configuration.instance.getDataDirectoryPath() + "/" + alias + "/" + f.getFilePath();
                    FileMetadata fileMetadata = FileMetadataService.findOrCreate(localFilePath);

                    // Check if the remote file is more recent
                    // We add 60_000 to the remote timestamp because the ftp server floors the time to the minutes
                    if (f.getLastModification() > fileMetadata.getLastModification()) {
                        System.out.println("Found a remote file change, updating...");
                        FlopboxService.downloadFile("ubuntu", f.getFilePath());
                    }
                }
                Thread.sleep(Configuration.instance.getPollTime() * 1000L);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
