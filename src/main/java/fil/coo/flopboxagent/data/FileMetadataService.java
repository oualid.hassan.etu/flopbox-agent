package fil.coo.flopboxagent.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import fil.coo.flopboxagent.configuration.Configuration;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FileMetadataService {
    public static Map<String, FileMetadata> FILES_METADATA;

    static {
        FILES_METADATA = new HashMap<>();
    }

    public static FileMetadata findOrCreate(String filePath) {
        File file = new File(filePath);
        if (FILES_METADATA.get(file.getAbsolutePath()) != null) {
            return FILES_METADATA.get(file.getAbsolutePath());
        }
        FILES_METADATA.put(file.getAbsolutePath(), new FileMetadata(0, file));
        return FILES_METADATA.get(file.getAbsolutePath());
    }

    public static void save() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(FILES_METADATA);
            FileOutputStream output = new FileOutputStream(System.getenv("APPDATA") + "/metadata.json");
            IOUtils.write(json, output);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setupMetadata(File directory) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File appdata = new File(System.getenv("APPDATA") + "/metadata.json");
        if (appdata.exists()) {
            FILES_METADATA = objectMapper.readValue(appdata, Map.class);
        } else {
            FILES_METADATA = new HashMap<>();
        }
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                setupMetadata(file);
                return;
            }
            if (FILES_METADATA.get(file.getAbsolutePath()) == null) {
                FILES_METADATA.put(file.getAbsolutePath(), new FileMetadata(0, file));
            }
        }
    }
}
