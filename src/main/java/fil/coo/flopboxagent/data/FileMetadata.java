package fil.coo.flopboxagent.data;

import java.io.File;
import java.util.Date;

public class FileMetadata {
    private long lastModification;
    private File file;

    public FileMetadata(long lastModification, File file) {
        this.lastModification = lastModification;
        this.file = file;
    }

    public void update() {
        lastModification = new Date().getTime();
    }

    public long getLastModification() {
        return lastModification;
    }

    public void setLastModification(long lastModification) {
        this.lastModification = lastModification;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
