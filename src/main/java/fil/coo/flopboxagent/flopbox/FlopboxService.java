package fil.coo.flopboxagent.flopbox;

import com.fasterxml.jackson.databind.ObjectMapper;
import fil.coo.flopboxagent.configuration.Configuration;
import fil.coo.flopboxagent.configuration.FlopboxServer;
import fil.coo.flopboxagent.configuration.LocalFtpServer;
import fil.coo.flopboxagent.data.FileMetadataService;
import okhttp3.*;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FlopboxService {
    private final static String SEPARATOR = Pattern.quote(System.getProperty("file.separator"));
    private static String JWT = "";

    public static void login() throws IOException {
        FlopboxServer flopboxServer = Configuration.instance.getFlopbox();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(new AuthDto(flopboxServer.getPassword(), flopboxServer.getUsername()));
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(flopboxServer.getHost() + "/v1/auth")
                .post(RequestBody.create(MediaType.parse("application/json"), json))
                .build();
        Response response = okHttpClient.newCall(request).execute();
        if (response.header("Set-Cookie") == null) {
            throw new RuntimeException(response.toString());
        }
        FlopboxService.JWT = response.header("Set-Cookie").split("=")[1].split(";")[0];
    }

    public static FtpServer[] getAliases() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://localhost:8080/v1/users/me/servers")
                .header("Authorization", "Bearer " + JWT)
                .build();
        Response response = okHttpClient.newCall(request).execute();
        FtpServer[] ftpServers = objectMapper.readValue(response.body().byteStream(), FtpServer[].class);
        return ftpServers;
    }

    /**
     * Uploads a file to the Flopbox server, extracting the serve's name from the path
     *
     * @param file
     */
    public static void upload(File file) {
        String alias = file.getAbsolutePath().replace(Configuration.instance.getDataDirectoryPath(), "")
                .split(SEPARATOR)[0];
        String remoteFilePath = file.getAbsolutePath().replace(Configuration.instance.getDataDirectoryPath(), "")
                .split(alias)[1]
                .substring(1);
        LocalFtpServer ftpServer = Configuration.getFtpServerFromAlias(alias).orElseThrow();

        RequestBody fileBody = RequestBody.create(file, MediaType.parse("text/html"));

        MultipartBody multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(), fileBody)
                .build();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(Configuration.instance.getFlopbox().getHost() + "/v1/ftps" + "/" + alias + "/" + remoteFilePath)
                .header("Authorization", "Bearer " + JWT)
                .header("Ftp-Username", ftpServer.getUsername())
                .header("Ftp-Password", ftpServer.getPassword())
                .put(multipartBody)
                .build();

        FileMetadataService.findOrCreate(file.getAbsolutePath()).update();
        try (Response response = client.newCall(request).execute()) {
            System.out.println("File uploaded : " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void downloadFolder(String alias, String path) throws IOException {
        LocalFtpServer ftpServer = Configuration.getFtpServerFromAlias(alias).orElseThrow();
        String rootPath = Configuration.instance.getDataDirectoryPath();

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://localhost:8080/v1/ftps/" + alias + "/" + path + "?action=download&recursive=true&type=zip")
                .header("Authorization", "Bearer " + JWT)
                .header("Ftp-Username", ftpServer.getUsername())
                .header("Ftp-Password", ftpServer.getPassword())
                .build();
        Response response = okHttpClient.newCall(request).execute();

        System.out.println("response = " + response);

        try (ZipInputStream zipInputStream = new ZipInputStream(response.body().byteStream())) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                String filePath = rootPath + "/" + alias + "/" + zipEntry.getName();
                File file = new File(filePath);
                file.getParentFile().mkdirs();
                System.out.println("file = " + file);
                if (zipEntry.isDirectory()) {
                    file.mkdir();
                } else {
                    try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
                        IOUtils.copy(zipInputStream, fileOutputStream);
                    }
                }
                FileMetadataService.findOrCreate(file.getAbsolutePath()).update();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static void downloadFile(String alias, String remoteFilePath) throws IOException {
        LocalFtpServer ftpServer = Configuration.getFtpServerFromAlias(alias).orElseThrow();
        String rootPath = Configuration.instance.getDataDirectoryPath();

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://localhost:8080/v1/ftps/" + alias + "/" + remoteFilePath + "?action=download&recursive=true&type=file")
                .header("Authorization", "Bearer " + JWT)
                .header("Ftp-Username", ftpServer.getUsername())
                .header("Ftp-Password", ftpServer.getPassword())
                .build();
        Response response = okHttpClient.newCall(request).execute();

        File targetFile = new File(rootPath + "/" + alias + "/" + remoteFilePath);
        targetFile.getParentFile().mkdirs();
        InputStream input = response.body().byteStream();
        FileOutputStream output = new FileOutputStream(targetFile);
        IOUtils.copy(input, output);
        input.close();
        output.close();
        FileMetadataService.findOrCreate(targetFile.getAbsolutePath()).update();
    }

    /**
     * Move a remote file to the deleted folder
     *
     * @param file
     */
    public static void moveToDeletedFolder(File file) {
        String alias = file.getAbsolutePath().replace(Configuration.instance.getDataDirectoryPath(), "")
                .split(SEPARATOR)[0];
        String remoteFilePath = file.getAbsolutePath().replace(Configuration.instance.getDataDirectoryPath(), "")
                .split(alias)[1]
                .substring(1);
        LocalFtpServer ftpServer = Configuration.getFtpServerFromAlias(alias).orElseThrow();

        OkHttpClient okHttpClient = new OkHttpClient();

        // Move local file located at "pathToFile" to ".deleted/" folder at ftp Server's root path
        Request request = new Request.Builder()
                .url("http://localhost:8080/v1/ftps/" + alias + "/" + remoteFilePath)
                .header("Authorization", "Bearer " + JWT)
                .header("Ftp-Username", ftpServer.getUsername())
                .header("Ftp-Password", ftpServer.getPassword())
                .post(RequestBody.create(MediaType.parse("application/json"), "/deleted/" + remoteFilePath))
                .build();

        FileMetadataService.findOrCreate(file.getAbsolutePath()).update();
        try (Response response = okHttpClient.newCall(request).execute()) {
            System.out.println("Moved file to deleted directory : " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get a list of files from a ftp server
     *
     * @param alias The ftp server's alias
     * @return A list of ftp files containing their full path and their last modification date
     * @throws IOException
     */
    public static FtpFileDto[] listFiles(String alias, String path) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        LocalFtpServer ftpServer = Configuration.getFtpServerFromAlias(alias).orElseThrow();

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://localhost:8080/v1/ftps/" + alias + "/" + path + "?action=list&recursive=true")
                .header("Authorization", "Bearer " + JWT)
                .header("Ftp-Username", ftpServer.getUsername())
                .header("Ftp-Password", ftpServer.getPassword())
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            return objectMapper.readValue(response.body().byteStream(), FtpFileDto[].class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
