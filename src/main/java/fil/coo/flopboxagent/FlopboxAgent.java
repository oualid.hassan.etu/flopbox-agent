package fil.coo.flopboxagent;

import fil.coo.flopboxagent.configuration.Configuration;
import fil.coo.flopboxagent.data.FileMetadataService;
import fil.coo.flopboxagent.data.LocalWatcherService;
import fil.coo.flopboxagent.data.RemoteWatcherService;
import fil.coo.flopboxagent.flopbox.FlopboxService;
import fil.coo.flopboxagent.flopbox.FtpServer;

import java.io.File;
import java.io.IOException;

public class FlopboxAgent {

    public static void main(String[] args) throws IOException {
        System.out.println("Flopbox agent started");
        FlopboxService.login();
        FtpServer[] aliases = FlopboxService.getAliases();
        LocalWatcherService.initializeDirectories(aliases);
        FileMetadataService.setupMetadata(new File(Configuration.instance.getDataDirectoryPath()));
        FileMetadataService.save();
        FlopboxService.downloadFolder("ubuntu", "/");

        new LocalWatcherService().start();
        new RemoteWatcherService().start();
    }
}
