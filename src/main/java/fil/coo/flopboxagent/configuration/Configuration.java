package fil.coo.flopboxagent.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

public class Configuration {
    private String dataDirectoryPath;
    private FlopboxServer flopbox;
    private LocalFtpServer[] servers;
    private String username;
    private String password;
    /**
     * The poll time (in seconds) for the remote watcher service
     */
    private int pollTime;

    public static final Configuration instance;

    static {
        Configuration configuration1;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            configuration1 = objectMapper.readValue(Configuration.class.getClassLoader().getResource("config.json"), Configuration.class);
        } catch (IOException e) {
            e.printStackTrace();
            configuration1 = new Configuration();
        }
        instance = configuration1;
    }

    public Configuration() {
    }

    public Configuration(String dataDirectoryPath, FlopboxServer flopbox, LocalFtpServer[] servers, String username, String password, int pollTime) {
        this.dataDirectoryPath = dataDirectoryPath;
        this.flopbox = flopbox;
        this.servers = servers;
        this.username = username;
        this.password = password;
        this.pollTime = pollTime;
    }

    public int getPollTime() {
        return pollTime;
    }

    public void setPollTime(int pollTime) {
        this.pollTime = pollTime;
    }

    public static Optional<LocalFtpServer> getFtpServerFromAlias(String alias) {
        return Arrays.stream(Configuration.instance.getServers())
                .filter(ftpServer -> ftpServer.getAlias().equals(alias))
                .findFirst();
    }

    public FlopboxServer getFlopbox() {
        return flopbox;
    }

    public void setFlopbox(FlopboxServer flopbox) {
        this.flopbox = flopbox;
    }

    public String getDataDirectoryPath() {
        return dataDirectoryPath;
    }

    public void setDataDirectoryPath(String dataDirectoryPath) {
        this.dataDirectoryPath = dataDirectoryPath;
    }

    public LocalFtpServer[] getServers() {
        return servers;
    }

    public void setServers(LocalFtpServer[] servers) {
        this.servers = servers;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
