package fil.coo.flopboxagent.configuration;

public class LocalFtpServer {
    private String alias;
    private String username;
    private String password;

    public LocalFtpServer() {
    }

    public LocalFtpServer(String alias, String username, String password) {
        this.alias = alias;
        this.username = username;
        this.password = password;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
