# SR2 - FlopBox-Agent

## Hassan Oualid - Morel Adrien

### 10 Avril 2022

## Introduction

Ce projet à pour but de permettre à un utilisateur une mise à jour automatique des fichiers de répertoires
associès avec un serveur FTP via l'api FlopBox. A la création ainsi qu'a la modification d'un fichier local celui-ci est envoyé sur le serveur FTP
associé a son répertoire. A la suppression celui-ci est déplacé vers un repertoire distant ".deleted" à la racine du serveur FTP.

## Démo

![](video.mp4)

## Exécution

Pour générer un exécutable .jar utilisez la commande suivante:

```bash
mvn clean package
```

Ensuite pour le lancer:

```bash
java -jar ./target/flopbox-agent-1.0-SNAPSHOT.jar
```

Pour générer la javadoc:

```bash
mvn javadoc:javadoc
```

## Architechture

Deux Threads distinct sont présent dans ce programme, un est chragé de verifier si il y a création, modification, suppression des fichiers locaux.
(Via la librairie Apache.io Monitor)
L'autre est lui chargé de vérifier si il y a des modfications sur le serveur FTP distant.

De plus voici le diagram uml du projet:

![](uml.png)

## Code samples


Ici pour verifier la modification des fichiers locaux nous utilisons Apache.io Monitor qui permet une compréhension aisée du code:

```java
public void run() {
        File directory = new File(Configuration.instance.getDataDirectoryPath());
        FileAlterationObserver observer = new FileAlterationObserver(directory);
        observer.addListener(this);
        while (true) {
            try {
                observer.checkAndNotify();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
```

Ici grace au "objectMapper" on peut transformer facilement en objet java les résultats d'une requête sur l'api:

```java
public static FtpFileDto[] listFiles(String alias, String path) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        LocalFtpServer ftpServer = Configuration.getFtpServerFromAlias(alias).orElseThrow();

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://localhost:8080/v1/ftps/" + alias + "/" + path + "?action=list&recursive=true")
                .header("Authorization", "Bearer " + JWT)
                .header("Ftp-Username", ftpServer.getUsername())
                .header("Ftp-Password", ftpServer.getPassword())
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            return objectMapper.readValue(response.body().byteStream(), FtpFileDto[].class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
```

Ici on récupere les séparateur présents dans le path des fichiers, cela permet l'intercompatibilité du code notament sous les systèmes windows:

```java
public class FlopboxService {
    private final static String SEPARATOR = Pattern.quote(System.getProperty("file.separator"));
    ...
}
```
